from webapp2 import Response
from ferris import Controller, messages, route, route_with
from app.errors import TicTacError
from app.models.board import Board
import json
import logging


class Boards(Controller):
    class Meta:
        Model = Board
        components = (messages.Messaging,)
        prefixes = ('api',)

    @route_with(template='/')
    def show(self):
        self.meta.view.template_name = 'angular/index.html'

    @route_with('/api/boards/new', methods=['POST'])
    def api_new(self):
        """Creates a new board and returns the key"""
        params = json.loads(self.request.body)
        self.context['data'] = Board.new(int(params['length']), int(params['width']))

    @route_with('/api/boards/:<key>/play', methods=['POST'])
    def api_play(self, key):
        """Modifies the board's tiles, hence, play"""
        params = json.loads(self.request.body)
        board = self.util.decode_key(key).get()
        try:
            board.square_is(int(params['x']), int(params['y']), params['tile'])
            return 200
        except TicTacError as e:
            logging.error(e)
            # build response
            resp = Response()
            resp.status_int = e.code
            resp.status_message = resp.body = e.description
            return resp

    @route
    def api_debug_play(self):
        """Can be used for simulating plays"""
        try:
            board = Board.new(3)
            # X WINS
            board.square_is(1, 1, 'X')
            board.square_is(1, 0, 'O')
            board.square_is(0, 1, 'X')
            board.square_is(2, 1, 'O')
            board.square_is(0, 2, 'X')
            board.square_is(2, 0, 'O')
            board.square_is(0, 0, 'X')
            # DRAW
            # board.square_is(0, 0, 'X')
            # board.square_is(1, 0, 'O')
            # board.square_is(2, 0, 'X')
            # board.square_is(0, 1, 'O')
            # board.square_is(1, 1, 'X')
            # board.square_is(2, 1, 'O')
            # board.square_is(0, 2, 'X')
            # board.square_is(1, 2, 'O')
            # board.square_is(2, 2, 'X')
            return 200
        except TicTacError as e:
            logging.error(e)
            # build response
            resp = Response()
            resp.status_int = e.code
            resp.status_message = resp.body = e.description
            return resp
