from ferris import BasicModel, ndb
from app.errors import InvalidMove, WinningMove, DrawMove
import logging


WIN_LENGTH = 3


class Board(BasicModel):
    matrix = ndb.PickleProperty()
    turn = ndb.IntegerProperty(default=0)
    finished = ndb.BooleanProperty(default=False)

    @classmethod
    def new(cls, length=3, width=None):
        if not length:
            raise Exception("Size can't be zero")
        if not width:
            width = length
        matrix = [['_' for _ in range(length)] for _ in range(width)]
        board = cls()
        board.populate(matrix=matrix)
        board.put()
        return board

    def square_is(self, x, y, tile):
        if self.matrix[y][x] != '_':
            raise InvalidMove()
        correct_tile = 'O' if self.turn % 2 else 'X'
        if tile != correct_tile:
            logging.warn("This is against the rules, %s" % tile)
        self.matrix[y][x] = tile
        self.turn += 1
        self.put()
        if self.is_won():
            self.finished = True
            self.put()
            raise WinningMove("%s has won" % tile)
        if self.turn == len(self.matrix) * len(self.matrix[0]):
            self.finished = True
            self.put()
            raise DrawMove()

    def is_won(self):
        checker = []
        # verify rows
        for row in self.matrix:
            checker = []
            for tile in row:
                if tile == '_' or (checker and tile != checker[-1]):
                    checker = []
                checker.append(tile)
                if len(checker) == WIN_LENGTH:
                    return True
        # verify columns
        rotated = zip(*self.matrix[::-1])
        for row in rotated:
            checker = []
            for tile in row:
                if tile == '_' or (checker and tile != checker[-1]):
                    checker = []
                checker.append(tile)
                if len(checker) == WIN_LENGTH:
                    return True
