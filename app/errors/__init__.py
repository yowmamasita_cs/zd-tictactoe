class TicTacError(Exception):
    def __init__(self, code, desc):
        self.code = code
        self.description = desc

    def __str__(self):
        return self.description


class InvalidMove(TicTacError):
    def __init__(self, desc=None):
        self.code = 405
        self.description = desc if desc else 'Move not allowed'


class WinningMove(TicTacError):
    def __init__(self, desc=None):
        self.code = 200
        self.description = desc if desc else 'YOU WON'


class DrawMove(TicTacError):
    def __init__(self, desc=None):
        self.code = 200
        self.description = desc if desc else 'Draw'
