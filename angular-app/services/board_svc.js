angular.module('app.services').
service('Board', function($log) {
    this.model = {};
    this.model.matrix = [];
    this.model.matrix_size = 3;
    this.model.in_progress = false;

    this.create_matrix = function() {
        this.model.matrix = [];
        var length = this.model.matrix_size;
        var width = this.model.matrix_size;
        for (i=0; i<width; i++) {
            this.model.matrix.push([]);
            for (j=0; j<length; j++) {
                this.model.matrix[i].push('_');
            }
        }
        this.model.in_progress = true;
    };
});
