angular.module('app.controllers').controller('MainCtrl', function($log, $scope, Board){

    $scope.model = Board.model;

    $scope.create_matrix = function() {
        Board.create_matrix();
    };
});
